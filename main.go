package main

import (
	"fmt"
	"gitlab.com/k33g/wasmedge-nanoservice/vmhelpers"
	"io/ioutil"
	"net/http"
)

func main() {

	wasmVM, _ := vmhelpers.InitializeVMFromWasmFile("hello-function/hello.wasm")

	service := func (response http.ResponseWriter, request *http.Request)  {
		body, _ := ioutil.ReadAll(request.Body)
		fmt.Println("body: " + string(body))

		defer request.Body.Close()

		result, _ := wasmVM.ExecuteFunction("handle", string(body))

		response.WriteHeader(http.StatusOK)
		response.Write([]byte(result))

	}

	http.HandleFunc("/", service)
	fmt.Println("Listening on 8080")
	http.ListenAndServe(":8080", nil)

	// curl -X POST -d '{"FirstName": "Bob", "LastName": "Morane"}' http://localhost:8080

}
