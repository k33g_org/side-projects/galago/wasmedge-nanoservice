# Hello function

## Helpers

```plantuml
class helpers {
  FromInt32PtrToString(value *int32) string
  FromStringToBytePtr(returnValue string) *byte
  Use(funktion func(body string) string, parameters *int32) *byte
}
```
